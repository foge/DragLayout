import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui';
import store from './store'//引入store
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,//使用store
  render: h => h(App)
})
