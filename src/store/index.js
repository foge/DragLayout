import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
    // 设置的全局访问的state对象
    state: {
        isEdit: false,
        draggable: true,
    },
    // 实时监听state值的变化（最新状态）
    getters: {},

    // 唯一允许更新state
    mutations: {
        setEdit(state, value) {
            state.isEdit = value;
        },
        setDrag(state, value) {
            state.draggable = value;
        },
    },
    // 触发更改
    actions: {}
});

export default store;